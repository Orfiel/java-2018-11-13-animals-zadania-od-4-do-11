package orfiel.model;

//publiczna klasa dziedzicząca po Lizard z zaimplementowanym interfejsem Herbivorous
public class Iguana extends Lizard implements Herbivorous{

    //publiczna metoda przyjmująca argumenty name, age, pscaleColor
    public Iguana(String name, int age, String scaleColor) {

        //wywołanie konstruktorów klasy nadrzędnej
        super(name, age, scaleColor);
    }

    //"nadpisanie" metody z interfejsu
    @Override
    public void eatingPlants() {
        System.out.println(getName() + " Żrę nasiona");

    }
    //dodanie nowej metody dla syczenia
    public void hiss(){
        System.out.println(getName() + " Syczę!!!!");
    }

    //nadpisanie metody z klasy Animal
    @Override
    public void eat() {
        System.out.println(getName() + " Żrę");
    }


}
