package orfiel.model;

//utworzenie publicznej klasy dziedziczacej po Animals
public abstract class Lizard extends Animal {

    //utworzenie zmiennej prywatnej
    private String scaleColor;

    //utworzenie get`erów i set`erów pozwalających pobierać wartości jak i je ustawiać
    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }

    //dodanie konstruktora z odziedziczonymi polami po Animals
    //jednocześnie dodanie pola z tej metody (scaleColor)
    public Lizard(String name, int age, String scaleColor) {
        super(name, age);
        this.scaleColor = scaleColor;
    }

    //nadpisanie metody eat z Animal
    @Override
    public void eat() {

    }
}
