package orfiel.model;

//publiczna klasa dziedzicząca po Mammal z zaimplementowanym interfejsem Carnivorous
public class Wolf extends Mammal implements Carnivorous {

    //publiczna metoda przyjmująca argumenty name, age, furColor
    public Wolf(String name, int age, String furColor) {

        //wywołanie konstruktorów klasy nadrzędnej
        super(name, age, furColor);
    }

    //"nadpisanie" metody z interfejsu
    @Override
    public void eatingMeat() {
        System.out.println(getName() + " Żrę mięso");
    }
    //dodanie nowej metody dla wycia
    public void howl(){
        System.out.println(getName() +" Wyję!!!!!");
    }
    //nadpisanie metody z klasy Animal
    @Override
    public void eat() {
        System.out.println(getName() + " Żrę");

    }



}

