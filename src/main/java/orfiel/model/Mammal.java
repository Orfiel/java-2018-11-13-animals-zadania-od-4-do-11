package orfiel.model;

//utworzenie publicznej klasy dziedziczacej po Animals
public abstract class Mammal extends Animal {

    //utworzenie zmiennej prywatnej
    private String furColor;

    //dodanie konstruktora z odziedziczonymi polami po Animals
    //jednocześnie dodanie pola z tej metody (FurColor)
    public String getFurColor() {
        return furColor;
    }

    //utworzenie get`erów i set`erów pozwalających pobierać wartości jak i je ustawiać
    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    public Mammal(String name, int age, String furColor) {
        super(name, age);
        this.furColor = furColor;
    }

    //nadpisanie metody eat z Animal
    @Override
    public void eat() {

    }
}


