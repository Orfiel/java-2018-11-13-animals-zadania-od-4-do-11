package orfiel.model;

import java.io.Serializable;
//utworzenie klasy sbstrakcyjnej Animal z imprementacją Serializable (żadnych metod, jedynie informacja dla JVM)
public abstract class Animal implements Serializable {

    //utworzenie zmiennych o polach prywatnych
    private String name;

    private int age;

    //informacja dla JVM o wersji
    static final long serialVersionUID = 1;

    //utworzenie konstruktora
    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //utworzenie get`erów i set`erów pozwalających pobierać wartości jak i je ustawiać
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //utworzenie metody abstrakcyjnej która nic nie zwraca
    public abstract void eat();


}
