package orfiel.model;

//publiczna klasa dziedzicząca po Bird z zaimplementowanym interfejsem Herbivorous
public class Parrot extends Bird implements Herbivorous{

    //publiczna metoda przyjmująca argumenty name, age, plumageColor
    public Parrot(String name, int age, String plumageColor) {

        //wywołanie konstruktorów klasy nadrzędnej
        super(name, age, plumageColor);
    }
    //"nadpisanie" metody z interfejsu
    @Override
    public void eatingPlants() {
        System.out.println(getName() +" Żrę roślinki");
    }
    //dodanie nowej metody dla ćwierkania
    public void tweet(){
        System.out.println( getName() +" Ćwierkam");
    }
    //nadpisanie metody z klasy Animal
    @Override
    public void eat() {
        System.out.println(getName() + " Żrę");

    }
}
