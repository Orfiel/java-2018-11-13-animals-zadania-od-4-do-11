package orfiel.model;

//utworzenie publicznej klasy dziedziczacej po Animals
public abstract class Bird extends Animal {

    //utworzenie zmiennej prywatnej
    private String plumageColor;

    //dodanie konstruktora z odziedziczonymi polami po Animals
    //jednocześnie dodanie pola z tej metody (plumageColor)
    public Bird(String name, int age, String plumageColor) {
        super(name, age);
        this.plumageColor = plumageColor;

    }
    //utworzenie get`erów i set`erów pozwalających pobierać wartości jak i je ustawiać
    public String getPlumageColor() {
        return plumageColor;
    }

    public void setPlumageColor(String plumageColor) {
        this.plumageColor = plumageColor;
    }

    //nadpisanie metody eat z Animal
    @Override
    public void eat() {

    }
}
