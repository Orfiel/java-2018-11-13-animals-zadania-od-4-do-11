package orfiel;

import orfiel.model.*;

import java.io.*;


//publiczna klasa
public class AnimalMain {

    //publiczna statyczna metoda main
    public static void main(String[] args) {

        System.out.println("Welcome to the ZOO");

        //utworzenie tablicy o typie "Animal" i nazwie "animals"
        Animal[] animals;

        //przypisanie do tablicy "animals" wartości z metody "read"
        animals = read(new File("bufet.txt"));

        //zapisanie wartości tablicy "animals" do pliku "bufet.txt" w plikach tymczasowych
        zapis(animals, new File("/tmp/bufet.txt"));

        //metoda drukująca tablice "animals"
        drukowanie(animals);

        //metoda wywołująca karmienie zwierząt
        karmienie(animals);

        //metoda wywołująca karmienie mięsem
        karmienieMiesem(animals);

        //metoda wywołująca karmienie roślinami
        karmienieZielskiem(animals);

        //metoda wywołująca wycie
        wycie(animals);

        //metoda wywołującasyczenie
        syczenie(animals);

        //metoda wywołująca ćwierkanie
        cwierkanie(animals);

        //metoda zapisująca w pliku binarnym pod nazwą "bufetBinarnue" w plikach tymczasowych
        zapisBinarny(animals, new File("/tmp/bufetBinarnie.txt"));

        //przypisanie wartości z metody do zmiennej typu "Animal"
        Animal[] animalBinary = odczytBinarny(new File("/tmp/bufetBinarnie.txt"));

        //metoda drukująca tablicę animalBinary
        drukowanie(animalBinary);

    }


    //metoda zwracająca tablicę typu "Animal", przyjmująca jako argument zmienna typu File.
    //w file znajdują się informacje o plik (ścieżka)
    public static Animal[] read(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String indexText = br.readLine();
            int index = Integer.parseInt(indexText);
            Animal[] animals = new Animal[index];
            for (int i = 0; i < index; i++) {
                String gatunek = br.readLine();
                String name = br.readLine();
                String ageText = br.readLine();
                int age = Integer.parseInt(ageText);
                String color = br.readLine();

                if (gatunek.equals("wolf")) {
                    Wolf wolf = new Wolf(name, age, color);
                    animals[i] = wolf;

                } else if (gatunek.equals("iguana")) {
                    Lizard lizard = new Iguana(name, age, color);
                    animals[i] = lizard;

                } else if (gatunek.equals("parrot")) {
                    Parrot parrot = new Parrot(name, age, color);
                    animals[i] = parrot;
                }
            }
            return animals;
        } catch (IOException ex) {
            System.err.print(ex);
            return new Animal[0];
        }
    }

    //metoda zapisująca tablicę zwierząt do plik
    public static void zapis(Animal[] animals, File file) {
        try (Writer fw = new BufferedWriter(new FileWriter(file))) {
            fw.write(animals.length + "\n");
            for (Animal animal : animals) {
                if (animal instanceof Wolf) {
                    Wolf wolf = (Wolf) animal;
                    fw.write("wolf" + "\n");
                    fw.write(wolf.getName() + "\n");
                    fw.write(wolf.getAge() + "\n");
                    fw.write(wolf.getFurColor() + "\n");
                } else if (animal instanceof Iguana) {
                    Iguana iguana = (Iguana) animal;
                    fw.write("iguana" + "\n");
                    fw.write(iguana.getName() + "\n");
                    fw.write(iguana.getAge() + "\n");
                    fw.write(iguana.getScaleColor() + "\n");
                } else if (animal instanceof Parrot) {
                    Parrot parrot = (Parrot) animal;
                    fw.write("parrot" + "\n");
                    fw.write(parrot.getName() + "\n");
                    fw.write(parrot.getAge() + "\n");
                    fw.write(parrot.getPlumageColor() + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //mwtoda wypisująca komplet informacji o każdym zwierzaku z talicy
    public static void drukowanie(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Wolf) {
                Wolf wolf = (Wolf) animal;
                System.out.print(wolf.getClass().getSimpleName() + " ");
                System.out.print(wolf.getName() + " ");
                System.out.print(wolf.getAge() + " ");
                System.out.println(wolf.getFurColor());
            } else if (animal instanceof Iguana) {
                Iguana iguana = (Iguana) animal;
                System.out.print(iguana.getClass().getSimpleName() + " ");
                System.out.print(iguana.getName() + " ");
                System.out.print(iguana.getAge() + " ");
                System.out.println(iguana.getScaleColor());
            } else if (animal instanceof Parrot) {
                Parrot parrot = (Parrot) animal;
                System.out.print(parrot.getClass().getSimpleName() + " ");
                System.out.print(parrot.getName() + " ");
                System.out.print(parrot.getAge() + " ");
                System.out.println(parrot.getPlumageColor());
            }
        }

    }
    //wywoładnie metody karmienia wszystkich zwierząt
    public static void karmienie(Animal[] animals) {
        for (Animal animal : animals) {
            animal.eat();
        }
        System.out.println();

    }
    //wywoładnie metody karmienia karmiącej mięsożerców
    public static void karmienieMiesem(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Carnivorous) {
                ((Carnivorous) animal).eatingMeat();
            }
        }
        System.out.println();
    }
    //wywoładnie metody karmienia karmiącej roślinożerców
    public static void karmienieZielskiem(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Herbivorous) {
                ((Herbivorous) animal).eatingPlants();
            }
        }
        System.out.println();
    }
    //wywołanie syczenia dla klas posiadających metodę "howl"
    public static void wycie(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Wolf) {
                ((Wolf) animal).howl();
            }
        }
        System.out.println();
    }
    //wywołanie syczenia dla klas posiadających metodę "hiss"
    public static void syczenie(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Iguana) {
                ((Iguana) animal).hiss();
            }
        }
        System.out.println();
    }

    //wywołanie ćwierkanie dla klas posiadjących metodę "tweet"
    public static void cwierkanie(Animal[] animals) {
        for (Animal animal : animals) {
            if (animal instanceof Parrot) {
                ((Parrot) animal).tweet();
            }
        }
        System.out.println();
    }
    //zapisuje tablicę typu "Animal" o nazwie "animals" do pliku binarnego (w sposób binarny)
    public static void zapisBinarny(Animal[] animals, File file) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(animals);
        } catch (IOException ex) {
            System.err.println(ex);
        }

    }

    //zwraca tablice typu "Animal" odczytaną z pliku binarnego
    public static Animal[] odczytBinarny(File file) {
        Animal[] animalBinary;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            animalBinary = (Animal[]) ois.readObject();

            return animalBinary;
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);

            return new Animal[0];
        }

    }
}